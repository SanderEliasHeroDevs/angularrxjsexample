import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {IUser} from '../../interfaces';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UsersComponent {
  @Input() users!: IUser[];
  @Output() whenUserSelected: EventEmitter<IUser> = new EventEmitter<IUser>();

  onClick(user: IUser): void {
    this.whenUserSelected.emit(user);
  }
}
