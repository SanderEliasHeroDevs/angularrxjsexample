import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {IPost} from '../../interfaces';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostsComponent {
  @Input() posts!: IPost[];
  @Output() whenPostSelected: EventEmitter<IPost> = new EventEmitter<IPost>();

  onClick(post: IPost): void {
    // @TODO: Do what you want here...
    this.whenPostSelected.emit(post);
  }
}

