import {Component, OnInit} from '@angular/core';
import {TypicodeService} from '../../services/typicode.service';
import {Observable, combineLatest, Subject, of, forkJoin} from 'rxjs';
import {distinctUntilChanged, map, switchMap} from 'rxjs/operators';
import {IData, IPost, ISelectedUser, IUser} from '../../interfaces';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  readonly subject: Subject<any> = new Subject();
  data$!: Observable<IData>;
  selectedUser$!: Observable<ISelectedUser>;

  constructor(private readonly typicodeService: TypicodeService) {
  }

  ngOnInit(): void {
    this.selectedUser$ = this.subject.pipe(
      distinctUntilChanged(),
      switchMap(user => forkJoin({
        user: of(user),
        posts: this.typicodeService.getPostsByUserId(user)
      }))
    );

    this.data$ = combineLatest(
      this.typicodeService.getUsers(),
      this.typicodeService.getPosts(),
      (users, posts) => ({users, posts})
    ).pipe(
      map((data: IData) => ({...data, posts: data.posts.slice(0, 10)}))
    );
  }

  onPostSelected(post: IPost): void {
    console.log('So you want to view:', post);
  }

  onUserSelected(user: IUser): void {
    this.subject.next(user);
  }
}
