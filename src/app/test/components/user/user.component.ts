import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {IPost, ISelectedUser} from '../../interfaces';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserComponent {
  @Input() user!: ISelectedUser;
  @Output() whenPostSelected: EventEmitter<IPost> = new EventEmitter<IPost>();

  onClick(post: IPost): void {
    this.whenPostSelected.emit(post);
  }
}
