import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './components/home/home.component';
import {RouterModule, Routes} from '@angular/router';
import {UserComponent} from './components/user/user.component';
import {UsersComponent} from './components/users/users.component';
import {PostsComponent} from './components/posts/posts.component';

const routes: Routes = [
  {path: '', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule],
  exports: [RouterModule, UserComponent, PostsComponent, UsersComponent],
  declarations: [UserComponent, UsersComponent, PostsComponent],
})
export class TestRoutingModule {
}

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    TestRoutingModule,
  ]
})
export class TestModule {
}
