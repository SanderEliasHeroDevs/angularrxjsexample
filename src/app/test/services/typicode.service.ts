import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IPost, IUser} from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class TypicodeService {

  constructor(private readonly httpClient: HttpClient) {
  }

  getUsers(): Observable<IUser[]> {
    return this.httpClient.get<IUser[]>('https://jsonplaceholder.typicode.com/users');
  }

  getPosts(): Observable<IPost[]> {
    return this.httpClient.get<IPost[]>(`https://jsonplaceholder.typicode.com/posts`);
  }

  getPostsByUserId(user: IUser): Observable<any> {
    return this.httpClient.get(`https://jsonplaceholder.typicode.com/posts?userId=${user.id}`);
  }
}
